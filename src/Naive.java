import java.util.*;

import myutil.*;

public class Naive {
	public static void main(String[] args){
		Data data = new Data();
		ArrayList<Integer> dimen = data.query.dimen;
		List<Integer> SkyLineSet = new ArrayList<Integer>();
		boolean isSkyline;
		ArrayList<ArrayList<Integer>> DominSet = new ArrayList<ArrayList<Integer>>();
		for(int i = 0; i < data.points.size(); i++){
			DominSet.add(new ArrayList<Integer>());
		}
		System.out.println(DominSet.size());
		//Start Timer-------------------
		long startTime = System.currentTimeMillis();
		for(int i = 0; i < data.points.size(); i++){
			isSkyline = true;
			for(int j = 0; j < data.points.size(); j++){
				if(Domin.isDominate(data.points.get(i), data.points.get(j), data.query)){
					isSkyline = false;
					(DominSet.get(j)).add(i);
					//break;
				}
			}
			if(isSkyline){
				SkyLineSet.add((int)data.points.get(i)[0]);
			}
		}
		//Start Timer-------------------
		long endTime = System.currentTimeMillis();
		long runTime = endTime - startTime; 
		Impure.printRes(SkyLineSet, runTime);
		System.out.println();
		for(int x: DominSet.get(5)){
			System.out.print(x+" ");
		}
		System.out.println();
		for(int i = 0; i < SkyLineSet.size(); i++){
			System.out.print(SkyLineSet.get(i)+":" +DominSet.get(SkyLineSet.get(i)).size() + " ");
		}
		System.out.println();
		int k = 15;
		ArrayList<Integer>S = new ArrayList<Integer>();
		ArrayList<Integer>St = new ArrayList<Integer>();
		for(int i = 0; i < k; i++){
			int maxi = 0, ind = 0;
			ArrayList<Integer> chos = new ArrayList<Integer>();
			for(int j = 0; j < SkyLineSet.size(); j++){
				ArrayList<Integer> st = uni(DominSet.get(SkyLineSet.get(j)), S);
				if(maxi < st.size()){
					maxi = st.size();
					chos = st;
					ind = j;
				}
			}
			S = chos;
			St.add(ind);
		}
		
		System.out.println(S.size());
		for(int x: S){
			System.out.print(x+" ");
		}
		System.out.print("\n");
		for(int x: St){
			System.out.print(x+" ");
		}
	}	
	public static ArrayList<Integer> uni(ArrayList<Integer> s1, ArrayList<Integer> S){
		int sz1 = s1.size();
		int sz2 = S.size();
		//System.out.println(sz1+" "+sz2);
		int i = 0, j = 0;
		ArrayList<Integer> fin = new ArrayList<Integer>();
		while(i != sz1 || j != sz2){
			//System.out.println(i+" "+j);
			if(i == sz1){
				fin.add(S.get(j));
				j++;
				continue;
			}
			if(j == sz2){
				fin.add(s1.get(i));
				i++;
				continue;
			}
			if(s1.get(i) < S.get(j)){
				fin.add(s1.get(i));
				i++;
			}else{
				if(s1.get(i) > S.get(j)){
					fin.add(S.get(j));
					j++;
				}else{
					fin.add(S.get(j));
					i++; j++;
				}
			}
		}
		return fin;
	}
}