import java.util.*;
import myutil.*;

public class SFS {
	public static void main(String[] args){
		Data data = new Data();													// Input scanned.
		long startTime = System.currentTimeMillis();							// TimeStamp start.
		data = calcEntropy(data);												// Calculate Entropy.
		int field = data.points.get(0).length;				
		data = sortEntropy(data, field-1);										// Sort Entropy.
		ArrayList<float[]> skylines = round(data);								// Calling BNL Rounds.
		long runTime = System.currentTimeMillis() - startTime;					// TimeStamp stopped.
		Impure.printRes(skylines, runTime);
	}
	
	public static ArrayList<float[]> round(Data data){
		Window window = new Window(data.query.WinSize);
		Disk disk = new Disk();
		window.addWin(data.points.get(0));
		ArrayList<float[]> skylines = new ArrayList<float[]>();  
		for(int i = 1; i < data.points.size(); i++){
			boolean isWin = true; 	
			for(int j = 0; j < window.win.size(); j++){
				if(Domin.isDominate(window.win.get(j), data.points.get(i), data.query)){
					window.rmWin(j);
					j--;
				}else{
					if(Domin.isDominate(data.points.get(i), window.win.get(j), data.query)){
						Domin.count--;
						isWin = false;
						break;
					}
				}
			}
			if(isWin){
				if(!window.isfull())
					window.addWin(data.points.get(i));
				else
					disk.addDisk(data.points.get(i));
			}
		}
		if(!disk.diskFile.isEmpty()){
			float[] x = disk.diskFile.get(0);
			for(int i = 0; i < window.win.size(); i++){
				float[] w = ((float[])window.win.get(i));
				if(x[x.length-1] > w[w.length-1]){
					skylines.add(w);
					window.rmWin(i);
					i--;
				}
			}
			data.points.clear();
			data.points.addAll(window.win);
			data.points.addAll(disk.diskFile);
			disk.clearDisk();
			window.clearWin();
			skylines.addAll(round(data));
			return skylines;
		}else{
			return window.win;
		}
	}
	
	public static Data calcEntropy(Data data){
		ArrayList<float[]> Cpoints = (ArrayList<float[]>)data.points.clone();
		data.points.clear();
		ListIterator Ipoints = Cpoints.listIterator();
		int leng = Cpoints.get(0).length + 1;
		while(Ipoints.hasNext()){
			float[] point = new float[leng];
			float[] row = (float[])Ipoints.next();
			double entrop = 0;
			point[0] = row[0];
			for(int i = 1; i < row.length; i++){
				entrop = entrop + Math.log(row[i] + 1);
				point[i] = row[i]; 
			}
			point[row.length] = (float)entrop;
			data.points.add(point);
		}
		return data;
	}
	
	public static Data sortEntropy(Data data, final int field){
		Collections.sort(data.points, new Comparator<float[]>() {
            public int compare(float[] point1, float[] point2) {
                return Float.compare(point1[field], point2[field]);
            }
        });
		return data;
	}
}