package myutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Data {
	public ArrayList<float[]> points = new ArrayList<float[]>();
	public Query query = new Query();
	public Data(){
		try{
			points = Impure.getinput();
			query = Impure.getQuery();
		}
		catch(IOException e){
			System.out.print(e);
		}
	}
}
