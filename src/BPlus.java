import java.util.*;

import myutil.*;

public class BPlus {
	public static void main(String[] args){
		Data data = new Data();
		long startTime = System.currentTimeMillis();
		data.points = makeIndexed(data.points);
		final int indim = data.points.get(0).length - 2;
		data = makeSorted(data, indim);
		ArrayList<float[]> skylines = new ArrayList<float[]>();
		float w = 100, index = 0;
		for(int i = 0; i < data.points.size(); i++){
			ArrayList<float[]> indexGroup = new ArrayList<float[]>(Arrays.asList(data.points.get(i)));
			index = indexGroup.get(0)[indim];
			if(i != data.points.size() - 1)
				while(indexGroup.get(0)[indim] == data.points.get(i+1)[indim]){
					indexGroup.add(data.points.get(i+1));
					i++;
					if(i == data.points.size()-1)
						break;
				}
			if(index > w){
				break;
			}
			indexGroup = compareSky(indexGroup, indexGroup, data.query);
			skylines.addAll(compareSky(skylines, indexGroup, data.query));
			w = Collections.min(skylines, new Comparator<float[]>() {
	            public int compare(float[] point1, float[] point2) {
	                return Float.compare(point1[indim+1], point2[indim+1]);
	            }
	        })[indim+1];
		}
		long runTime = System.currentTimeMillis() - startTime;
		Impure.printRes(skylines, runTime);
	}
	
	public static ArrayList<float[]> makeIndexed(ArrayList<float[]> pts){
		ArrayList<float[]> newPts = new ArrayList<float[]>();
		int index = pts.get(0).length;
		int w = index + 1;
		ListIterator piter = pts.listIterator();
		while(piter.hasNext()){
			float[] older = (float[])piter.next();
			float[] newer = new float[older.length + 2];
			newer[0]= older[0];
			float min = older[1], max = older[1]; 
			for(int i = 1; i < older.length; i++){
				newer[i] = older[i];
				if(min > older[i]){
					min = older[i];
				}else{
					if(max < older[i]){
						max = older[i];
					}
				}
			}
			newer[index] = min;
			newer[w] = max;
			newPts.add(newer);
		}
		return newPts;
	}
	
	public static Data makeSorted(Data data, final int index){
		Collections.sort(data.points, new Comparator<float[]>() {
            public int compare(float[] point1, float[] point2) {
                return Float.compare(point1[index], point2[index]);
            }
        });
		return data;
	}
	
	public static ArrayList<float[]> compareSky(ArrayList<float[]> skylines, ArrayList<float[]> Group, Query q){
		ArrayList<float[]> newSkylines = new ArrayList<float[]>();
		for(int i = 0; i < Group.size(); i++){
			boolean isSkyline = true;
			for(int j = 0; j < skylines.size(); j++){
				if(Group.get(i)[0] != skylines.get(j)[0])
					if(Domin.isDominate(Group.get(i), skylines.get(j), q)){
						isSkyline = false;
						break;
					}
			}
			if(isSkyline){
				newSkylines.add(Group.get(i));
			}
		}		
		return newSkylines;
	}
}
