import java.util.*;

import myutil.*;

public class BNL {
	public static void main(String[] args){
		Data data = new Data();										// Input scanned.
		long startTime = System.currentTimeMillis();       			// TimeStamp start.
		ArrayList<float[]> SkylineSet = round(data); 				// Calling BNL Rounds.
		long runTime = System.currentTimeMillis() - startTime;		// TimeStamp stopped.
		Impure.printRes(SkylineSet, runTime);
	}
	
	public static ArrayList<float[]> round(Data data){                   
		Window window = new Window(data.query.WinSize);             // Window Created.
		Disk disk = new Disk();										// Disk file created.(imitation)
		window.addWin(data.points.get(0));
		ArrayList<float[]> skylines = new ArrayList<float[]>();  	
		for(int i = 1; i < data.points.size(); i++){				// scan through all data points.
			boolean isWin = true; 	
			for(int j = 0; j < window.win.size(); j++){				// scan through all window points.
				if(Domin.isDominate(window.win.get(j), data.points.get(i), data.query)){    // Check if i(data point) dominates j(window point). 
					window.rmWin(j);                                // remove point from window if i dominates j.
					j--;
				}else{
					if(Domin.isDominate(data.points.get(i), window.win.get(j), data.query)){   //check if j(window point) dominates i(data point).
						Domin.count--;
						isWin = false;								// specify it's not a window material. 
						break;										// i gets dominates so stop moving further. 
					}
				}
			}
			if(isWin){												// if window material. 				
				if(!window.isfull())								// if window is full add it to Window else add it to disk.
					window.addWin(data.points.get(i));
				else
					disk.addDisk(data.points.get(i));
			}
		}
		if(!disk.diskFile.isEmpty()){								
			float[] x = disk.diskFile.get(0);						// if Disk file is not empty. choose first element(having smallest time stamp) 
			for(int i = 0; i < window.win.size(); i++){				// go through all points from window and output as skyline if the point have smaller time stamp.
				float[] w = ((float[])window.win.get(i));
				if(x[0] > w[0]){
					skylines.add(w);
					window.rmWin(i);
					i--;
				}/*else{
					break;
				}*/
			}
			data.points.clear();															
			data.points.addAll(window.win);							// add window points that cannot be output to points for new round.
			data.points.addAll(disk.diskFile);						// add points from disk file to points.					
			disk.clearDisk();
			window.clearWin();
			//---------------------------------------
			Collections.sort(data.points, new Comparator<float[]>() {         // sort points so that time stamp agrees with ids. 
	            public int compare(float[] point1, float[] point2) {
	                return Float.compare(point1[0], point2[0]);
	            }
	        });
			//---------------------------------------
			skylines.addAll(round(data));							// call round with new data.
			return skylines;
		}else{
			return window.win;										// if Disk file is empty then output window points as skyline.
		}

	}
}